<?php
    function queryDB($query)
	{
		$connect = mysqli_connect("localhost", "root", "filicube", "issuer_dashboard");
		$filter_Result = mysqli_query($connect, $query);

		if($filter_Result)
			return $filter_Result;
		else
			die(mysqli_error($connect));
    }
    
    $query = "SELECT issuer.id, issuer.name, issuer.status_id, status.name as status FROM issuer JOIN status ON issuer.status_id = status.id;";
    $search_result = queryDB($query);
    
    $query_status = "SELECT * FROM status;";
    $status = queryDB($query_status);
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Local CSS -->
    <link rel="stylesheet" href="css/emi.css">

    <!-- Font Awesome -->
    <link href="css/fontawesome/css/all.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <!-- Local JS -->
    <script src="js/main.js"></script>
    <script src="js/sorttable.js"></script>
    
    <link rel="shortcut icon" href="assets/favicon.png" />
    <title>Emissores</title>
</head>

<body>
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.php">
            <img src="assets/only_logo.png" width="61" height="40" alt="" loading="lazy">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="emi_list.php">Emissores</a>
            </li>
          </ul>
        </div>
    </nav>

    <!-- HEADER -->
    <div class="main_content" class="container">
        <div class="row justify-content-md-center">
            <h1> Emissores </h1>
        </div>

        <!-- MAIN -->
        <div class="row justify-content-md-center">

            <div class="input-group mb-3">
                <input type="text" id="myInput" class="input form-control" placeholder="Filtrar" title="Type in a name">
            </div>

            <form id="table_form" method="post" action="php/updateOrRemoveEmissor.php">
                <table class="table table-hover sortable" style="text-align: center;">
                    <thead>
                        <tr>
                            <th class="order-by-desc" scope="col">#</th>
                            <th class="order-by-desc" scope="col">Nome</th>
                            <th class="order-by-desc" scope="col">Status</th>
                            <th scope="col">Editar</th>
                        </tr>
                    </thead>
                    <tbody id="emiTable">
                        <?php while($row = mysqli_fetch_array($search_result)):?>
                            <tr class="dialog" class="clickable">
                                <td class="emi_td"><p><?php echo $row['id'];?></p></td>
                                <td class="emi_td"> <p id="name_<?php echo $row['id'];?>"><?php echo $row['name'];?></p></td>
                                <td><p id="status_<?php echo $row['id'];?>"><?php echo $row['status'];?></p>
                                    <select id="status_select_<?php echo $row['id'];?>" class="form-control" name="status_id_<?php echo $row['id'];?>" hidden="true">
                                        <?php while($status_array = mysqli_fetch_array($status)) { $status_rows[] = $status_array;?>
                                        <?php } ?>
                                        <?php foreach ($status_rows as $status_row) { 
                                                if ($status_row['id'] == $row['status_id']) {
                                                    echo "<option value=". $status_row['id'] ." selected>". $status_row['name']. "</option>";
                                                }
                                                else {
                                                    echo "<option value=". $status_row['id'] .">". $status_row['name']. "</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </td>
                                <td style="text-align: center;">
                                    <button id="edit_btn_<?php echo $row['id'];?>" name="edit" type="button" value="<?php echo $row['id'];?>" onclick="editRow(<?php echo $row['id'];?>)" class="edit_button">
                                        <i class="fas fa-pencil-alt" style="color: white;"></i>
                                    </button>
                                    <button id="remove_btn_<?php echo $row['id'];?>" name="remove" value="<?php echo $row['id'];?>" type="submit" class="edit_button">
                                        <i class="fas fa-trash-alt" style="color: white;"></i>
                                    </button>
                                </td>
                            </tr>
                        <?php endwhile;?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>

</body>

</html>