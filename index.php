<?php
    function queryDB($query)
	{
		$connect = mysqli_connect("localhost", "root", "filicube", "issuer_dashboard");
		$filter_Result = mysqli_query($connect, $query);

		if($filter_Result)
			return $filter_Result;
		else
			die(mysqli_error($connect));
    }
    
    $query = "SELECT * FROM status;";
	$status = queryDB($query);
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    
    <!-- Locals -->
    <script src="js/main.js"></script>
    <link rel="stylesheet" href="css/main.css">
    
    <link rel="shortcut icon" href="assets/favicon.png" />
    <title>Home</title>
  </head>
  <body>
    
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.php">
            <img src="assets/only_logo.png" width="61" height="40" alt="" loading="lazy">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="emi_list.php">Emissores</a>
            </li>
          </ul>
        </div>
    </nav>

    <!-- HEADER -->
    <div class="container main_content">
        <div class="row justify-content-md-center">
            <h1 style="margin-bottom: 20px;"> Adicionar Emissor </h1>
            <br>
        </div>

        <!-- MAIN -->
        <div class="row justify-content-md-center">
            <form id="signupForm" method="post" action="php/postEmissor.php" accept-charset="UTF-8" validate>
                <div class="input-group input_div">
                    <span class="input-group-addon"></span>
                    <input id="name" class="form-control input-lg" placeholder="Nome" maxlength="100" type="text" name="name" required>
                </div>
                <div class="input-group input_div">
                    <span class="input-group-addon"></span>
                    <select class="form-control" name="status_id">
                        <?php while($row = mysqli_fetch_array($status)):?>
                            <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                        <?php endwhile;?>
                    </select>
                    <!-- <input id="status_id" class="form-control input-lg" placeholder="Status" maxlength="100" type="number" name="status_id"> -->
                </div>
                <div class="button-container-div">
                    <button class="btn btn-primary form_button" type="submit">Adicionar</button>
                </div>
            </form>
        </div>
    </div>

  </body>
</html>