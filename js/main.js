$(document).ready(function () {
    //Search table
    $("#myInput").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#emiTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

$(window).on("load resize ", function () {
    var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
    $('.tbl-header').css({ 'padding-right': scrollWidth });
}).resize();

function editRow(id) {
    var name = document.getElementById("name_" + id);
    var status = document.getElementById("status_" + id);
    var edit_btn = document.getElementById("edit_btn_" + id);
    var remove_btn = document.getElementById("remove_btn_" + id);
    var form = document.getElementById("table_form");

    if (name) {
        // Get its parent
        parent = name.parentNode;

        //If does not exist, create. Else, just show
        if (!document.getElementById("edit_name_" + id)) {
            const new_element = document.createElement('input');
            new_element.id = "edit_name_" + id;
            new_element.className = "form-control input-sm inner_table_input";
            new_element.type = "text";
            new_element.name = "edit_name_" + id;
            new_element.value = name.textContent;

            parent.insertBefore(new_element, name);
        }
        else {
            document.getElementById("edit_name_" + id).hidden = false;
        }
        name.hidden = true;

        // <button id="edit_btn_<?php echo $row['id'];?>" onclick="editRow(<?php echo $row['id'];?>)" class="edit_button">
        //     <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        //         <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
        //     </svg>
        // </button>

        //If does not exist, create. Else, just show
        if (!document.getElementById("ok_btn_" + id)) {
            const ok_btn = document.createElement('button');
            ok_btn.id = "ok_btn_" + id;
            ok_btn.name = "ok_btn_" + id;
            ok_btn.className = "edit_button";
            ok_btn.style = "color: white";
            ok_btn.addEventListener("click", function () {
                form.submit();
            });

            const input_id = document.createElement('input');
            input_id.name = "id";
            input_id.type = "text";
            input_id.value = id;
            input_id.hidden = true;
            form.appendChild(input_id);

            const ok_i = document.createElement('i');
            ok_i.className = "fas fa-check-circle";

            ok_btn.appendChild(ok_i);

            const cancel_btn = document.createElement('button');
            cancel_btn.id = "cancel_btn_" + id;
            cancel_btn.type = "button";
            cancel_btn.className = "edit_button";
            cancel_btn.style = "color: white";
            cancel_btn.setAttribute("onclick", "cancelBtn(" + id + ")");

            const cancel_i = document.createElement('i');
            cancel_i.className = "fas fa-ban";

            cancel_btn.appendChild(cancel_i);

            edit_btn_parent = edit_btn.parentNode;
            edit_btn_parent.insertBefore(cancel_btn, edit_btn);
            edit_btn_parent.insertBefore(ok_btn, cancel_btn);
        }
        else {
            document.getElementById("cancel_btn_" + id).hidden = false;
            document.getElementById("ok_btn_" + id).hidden = false;
        }
        edit_btn.hidden = true;
        remove_btn.hidden = true;
    }

    if (status) {
        document.getElementById("status_" + id).hidden = true;
        document.getElementById("status_select_" + id).hidden = false;
    }
}

function cancelBtn(id) {
    document.getElementById("edit_name_" + id).hidden = true;
    document.getElementById("name_" + id).hidden = false;
    document.getElementById("cancel_btn_" + id).hidden = true;
    document.getElementById("ok_btn_" + id).hidden = true;
    document.getElementById("edit_btn_" + id).hidden = false;
    document.getElementById("remove_btn_" + id).hidden = false;

    document.getElementById("status_" + id).hidden = false;
    document.getElementById("status_select_" + id).hidden = true;
}